import 'package:flutter/material.dart';

void main() =>runApp(HelloFlutteApp());

class HelloFlutteApp extends StatefulWidget{
  @override
  _HelloFlutteAppState createState() => _HelloFlutteAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String laosGreeting = "Sabaidee Flutter";
String koreaGreeting = "Anyoung Flutter";
class _HelloFlutteAppState extends State<HelloFlutteApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text(
                "Hello Siri"
            ),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting?spanishGreeting:englishGreeting;
                    });
                  },
                  icon: Icon(Icons.refresh)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting?laosGreeting:englishGreeting;
                    });
                  },
                  icon: Icon(Icons.abc_sharp)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting?koreaGreeting:englishGreeting;
                    });
                  },
                  icon: Icon(Icons.accessibility))
            ],
          ),
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 24),),
          ),
        )
    );
  }
}

// class HelloFlutteApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//             appBar: AppBar(
//               title: Text(
//                   "Hello Siri"
//               ),
//               leading: Icon(Icons.home),
//               actions: <Widget>[
//                 IconButton(
//                     onPressed: (){},
//                     icon: Icon(Icons.refresh))
//               ],
//             ),
//           body: Center(
//             child: Text(
//             "Hello Flutter",
//               style: TextStyle(fontSize: 24),),
//           ),
//         )
//     );
//   }
// }
